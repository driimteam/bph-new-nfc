var faq = [
    {
        question: "Co to jest NFC?",
        answer: "NFC (z ang. Near Field Communication – Komunikacja Bliskiego Pola) jest technologią komunikacji umożliwiająca transfer między urządzeniami znajdującymi się w&nbsp;odległości do 20 cm. Wykorzystuje się ją np. w&nbsp;kartach miejskich, kartach dostępowych, a&nbsp;w&nbsp;bankowości została zastosowana w&nbsp;usługach płatniczych przez telefony komórkowe.",
        parent: "#faq-one"
    },
    {
        question: "Gdzie mogę dokonywać płatności telefonem?",
        answer: "Płatności za pomocą telefonu można dokonywać wszędzie tam, gdzie widnieje symbol MasterCard PayPass&trade;.",
        parent: "#faq-one"
    },    
    {
        question: "Czy płatności NFC są dostępne dla klientów z&nbsp;ofertą operatora pre-paid?",
        answer: "Jest to zależne od oferty operatora. Orange umożliwia taką opcję, jednak musisz posiadać odpowiedni telefon, natomiast T&#8209;Mobile aktualnie nie umożliwia płatności NFC, jeśli korzystasz z&nbsp;oferty pre&#8209;paid.",
        parent: "#faq-one"
    },    
    {
        question: "Ile kosztuje karta NFC?",
        answer: "Karta NFC w&nbsp;Banku BPH jest oferowana bez żadnych opłat za jej wydanie, używanie (promocja trwa do 22.03.2016 r.), dostarczenie kodu PIN czy ponowne wydanie karty. Dla kart kredytowych NFC zachowane zostały ceny za przelewy i&nbsp;inne typowe dla rachunku transakcje niepowiązane z&nbsp;codziennymi zakupami, a standardowo naliczane przy kartach plastikowych. Po okresie promocji miesięczna opłata w&nbsp;wysokości 3 zł nie będzie pobierania o&nbsp;ile dokonasz 4 transakcji kartą NFC w&nbsp;danym miesiącu. Zmienne oprocentowanie Limitu kredytu wykorzystanego na Transakcje bezgotówkowe – wynosi 11,70% w&nbsp;stosunku rocznym (wg stanu na 01.01.2015 r.). Całkowita kwota Limitu kredytu wynosi 1&nbsp;371 zł. Rzeczywista roczna stopa oprocentowania (wyliczona przy założeniu, że Limit kredytu udzielany jest na dwunastomiesięczny okres trwania Umowy i&nbsp;spłacany jest w&nbsp;wysokości Minimalnej wymaganej miesięcznej spłaty, z&nbsp;wyjątkiem ostatniej płatności, która pokrywa kapitał pozostający do spłaty, odsetki oraz prowizję za udostępnienie. Limitu kredytu i&nbsp;obsługę Karty plastikowej) wynosi 24,63%. Całkowity koszt kredytu uwzględnia odsetki w&nbsp;wysokości 147,36 zł oraz prowizję za udostępnienie Limitu kredytu i&nbsp;obsługę Karty plastikowej w&nbsp;łącznej wysokości 120 zł. Powyższe wyliczenia zostały przygotowane na podstawie warunków, na jakich Bank spodziewa się zawrzeć 2/3 umów o&nbsp;kartę kredytową MasterCard/VISA Fair Banku BPH.",
        parent: "#faq-three"
    },
    {
        question: "Jak mogę aktywować zainstalowaną kartę NFC?",
        answer: "Kartę NFC możesz aktywować w&nbsp;dowolnym momencie (podobnie jak kartę plastikową):<br/><ul><li>w serwisie internetowym banku</li><li>dokonując transakcji potwierdzonej kodem PIN (transakcja powyżej 50 zł)</li><li>na Infolinii Banku BPH pod numerem 801&nbsp;889&nbsp;889 lub 58&nbsp;300&nbsp;75&nbsp;00 (z telefonu komórkowego lub z&nbsp;zagranicy).</li>",
        parent: "#faq-three"
    },
    {
        question: "Mam problem przy instalacji karty NFC, co mam robić?",
        answer: "Sprawdź, czy:<br/><ol><li>telefon jest wyposażony w&nbsp;funkcjonalność NFC</li><li>telefon jest na liście telefonów certyfikowanych przez operatorów – sprawdź na stronie <a href=\"http://www.orange.pl/nfcpass\" target=\"_blank\">www.orange.pl/nfcpass</a> lub T&#8209;Mobile</li><li>telefon ma wymaganą wersję oprogramowania firmowego (firmware)</li><li>karta SIM jest kartą SIM NFC - na karcie SIM jest wydrukowany napis zaczynający się od \"NFC\", np. \"NFC20\"</li><li>co najmniej raz otworzyłeś aplikację NFC Pass/MyWallet (z włączoną transmisją danych przez sieć komórkową, nie przez WiFi!) i&nbsp;zaakceptowałeś regulamin usługi</li><li>NFC Pass/MyWallet nie wyświetla komunikatów o&nbsp;błędzie</li><li>przeszedłeś proces instalacji karty NFC</li></ol>Jeżeli wszystkie powyższe warunki zostały spełnione, a problem z&nbsp;instalacją nadal występuje skontaktuj się z&nbsp;Infolinią Banku BPH, aby uzyskać pomoc.",
        parent: "#faq-three"
    },
    {
        question: "Jak odzyskać utracony PIN?",
        answer: "W przypadku konieczności wydania nowego kodu PIN dla karty NFC prosimy o&nbsp;telefon na Infolinię Banku. Proces wydania nowego PIN-u poprzez wysłanie wiadomości SMS zostanie przeprowadzony podczas jednej rozmowy z&nbsp;operatorem.",
        parent: "#faq-three"
    },
    {
        question: "Jak zastrzec kartę?",
        answer: "Kartę NFC zastrzega się identycznie jak każdą inną kartę plastikową, poprzez kontakt z&nbsp;Infolinią Banku 801 889 889 lub 58 300 75 00 (z telefonu komórkowego lub z&nbsp;zagranicy) lub w&nbsp;inny sposoby, wskazany w&nbsp;regulaminie kart płatniczych.",
        parent: "#faq-three"
    },
    {
        question: "Czy mogę robić przelewy z&nbsp;karty NFC?",
        answer: "Tak, możesz.",
        parent: "#faq-three"
    },
    {
        question: "Czy kartą NFC mogę zapłacić za granicą?",
        answer: "Tak, kartą NFC możesz płacić zbliżając telefon do dowolnego czytnika, który akceptuje transakcje zbliżeniowe (jeśli widać logo PayPass na urządzeniu).",
        parent: "#faq-three"
    },
    {
        question: "Co to jest karta NFC?",
        answer: "Karta NFC to rodzaj wirtualnej karty płatniczej zainstalowanej na karcie SIM telefonu.",
        parent: "#faq-two"
    },    
    {
        question: "Czy muszę posiadać specjalny telefon?",
        answer: "Karta NFC działa w&nbsp;telefonach wyposażonych w&nbsp;antenę NFC.<br/>Aktualną ofertę telefonów z&nbsp;NFC można znaleźć na stronie <a href=\"http://www.orange.pl/nfc\" target=\"_blank\">www.orange.pl/nfc</a>, pod zakładką eSklep lub na <a href=\"http://www.t-mobile.pl/pl/mywallet\" target=\"_blank\">http://www.t-mobile.pl/pl/mywallet</a>.",
        parent: "#faq-two"
    },    
    {
        question: "Co jest niezbędne, by korzystać z&nbsp;karty NFC?",
        answer: "Warunkiem otrzymania karty NFC jest:<br/><ol><li>zawarcie umowy z&nbsp;Bankiem BPH o&nbsp;wydanie karty płatniczej w&nbsp;formie karty NFC w&nbsp;dogodnym dla Ciebie miejscu (umowę możesz zawrzeć w&nbsp;placówce Banku lub przez system internetowy Banku, o&nbsp;ile posiadasz aktywny dostęp do kanałów elektronicznych Banku BPH)</li><li>posiadane aktywnej umowy z&nbsp;operatorem (T&#8209;Mobile lub Orange)</li><li>posiadanie karty SIM NFC</li><li>włączony transfer danych komórkowych</li><li>zainstalowana aplikacja płatnicza: MyWallet lub BPH NFC (w przypadku pierwszego użycia karty NFC należy dodać kartę do aplikacji BPH NFC w&nbsp;aplikacji Orange NFC Pass)</li></ol>",
        parent: "#faq-two"
    },       
    {
        question: "Czy jeśli posiadam już kartę plastikową, to czy mogę zamówić kartę NFC?",
        answer: "Tak, Bank BPH oferuje wszystkim swoim klientom posiadającym karty plastikowe MasterCard możliwość zawnioskowania o&nbsp;kartę NFC. Aby zawnioskować o&nbsp;kartę udaj się do placówki Banku BPH lub aplikuj przez system internetowy Banku, o&nbsp;ile posiadasz aktywny dostęp do kanałów elektronicznych Banku BPH.",
        parent: "#faq-four"
    },
    {
        question: "Jak wygląda proces dostarczenia PIN-u za pomocą SMS-a?",
        answer: "<ol><li>Bank po udanej instalacji karty NFC wyśle do Ciebie wiadomość SMS inicjującą pobranie kodu PIN do karty</li><li>musisz odesłać odpowiedź SMS-em na wskazany numer z&nbsp;otrzymanym z&nbsp;Banku hasłem (SMS z&nbsp;hasłem musi być wysłany z&nbsp;numeru telefonu, jaki został podany podczas procesu wnioskowania)</li><li>po weryfikacji poprawności hasła (możliwe 3 próby), otrzymasz kolejną wiadomość SMS z&nbsp;kodem PIN</li></ol>W celu zachowania bezpieczeństwa powinieneś zapamiętać kod PIN i&nbsp;skasować SMS. W trosce o&nbsp;bezpieczeństwo Bank może podjąć próbę skasowania SMS-a z&nbsp;telefonu.",
        parent: "#faq-four"
    },
    {
        question: "Do kogo mam zgłosić problem z&nbsp;funkcjonalnością NFC w&nbsp;moim telefonie?",
        answer: "Jeżeli masz problem z&nbsp;funkcjonalnością NFC, upewnij się, że jest włączona taka funkcja w&nbsp;telefonie. Jeżeli antena NFC jest włączona, a Ty nie możesz dokonywać płatności zbliżeniowych: skontaktuj się ze swoim operatorem T Mobile/Orange, w&nbsp;celu rozwiązania problemu z&nbsp;telefonem. Jeśli telefon został kupiony u innego dostawcy, skontaktuj się ze sprzedawcą lub wskazanym przez niego serwisem.",
        parent: "#faq-four"
    },
    {
        question: "Co mam zrobić w&nbsp;przypadku kradzieży telefonu?",
        answer: "W przypadku kradzieży bądź zagubienia telefonu niezwłocznie poinformuj Bank i&nbsp;zastrzeż kartę NFC. Zablokowanie rachunku karty zabezpieczy środki przed kradzieżą. Dodatkowo Bank BPH podejmie próbę usunięcia karty NFC z&nbsp;karty SIM telefonu, jeśli jest on nadal zalogowany do sieci.",
        parent: "#faq-four"
    },
    {
        question: "Czy będę widział kartę NFC w&nbsp;systemie internetowym Banku?",
        answer: "Tak. Karta NFC będzie widoczna w&nbsp;systemie internetowym Banku oraz serwisie bankowości mobilnej po zalogowaniu się na swoje konto.",
        parent: "#faq-four"
    },
    {
        question: "Czy mogę płacić kartą w&nbsp;internecie?",
        answer: "Tak, możesz płacić kartą NFC w&nbsp;internecie. Karta NFC, nie różni się pod tym względem niczym od karty plastikowej.",
        parent: "#faq-four"
    }
];