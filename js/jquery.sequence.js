/**
 * Sequence plugin
 * 
 * @author: Krzysztof Pintscher
 * @company: Driim Interactive
 * @version: 0.8 beta
 */

(function ($) {

    var Sequence = function (element, options) {
        this.element = $(element);
        this.options = options;
        this.images = $('<ul></ul>');
        this.canvas = false;
        this.frames = [];
        this.loadedImages = 0;
        this.currentFrame = 0;
        this.interval = 0;
        this.animateTo;
        this.loader = false;
        this.init(this.element);
    };

    Sequence.defaults = {
        path: '/img/',
        name: 'seq_',
        toLoad: 0,
        start: 0,
        numpad: 0,
        extension: 'jpg',
        method: 'loop',
        className: 'seq',
        classSeparator: '_',
        classGlobal: 'sequence',
        classCommon: 'sequence',
        classPad: 0,
        responsive: false,
        height: 100,
        width: 100,
        files: [],
        render: 'html',
        loader: false,
        callback: false,
        background: false,
        firstFrame: 0
    };

    Sequence.prototype.init = function (element) {
        var sequence = this;
        if (this.options.loader !== false) {
            sequence.showLoader();
        }
        if (this.options.render === 'html') {
            var $images = this.images;
            element.append($images);
        } else if (this.options.render === 'canvas') {
        }
        if (this.options.method === 'sprite') {
            if (this.options.files.length > 0) {
                sequence.loadSprite(0);
            }
        } else if (this.options.method === 'loop') {
            sequence.loadImages(this.options.start);
        } else {
            console.log('Please use "sprite" or "loop" load method.');
        }
    };

    Sequence.prototype.showLoader = function () {
        var image = new Image();
        image.src = this.options.loader;

        var $loader = $('<div></div>');
        if (this.options.background) {
            $loader.css({
                'background': this.options.background,
                'position': 'absolute',
                'width': '100%',
                'height': '100%',
                'z-index': '999'
            });
        }

        var $image = $('<img>').attr('src', this.options.loader);
        $image.css({
            'position': 'absolute',
            'top': '50%',
            'left': '50%',
            'margin-left': (image.width / 2) * (-1),
            'margin-right': (image.height / 2) * (-1)
        });
        $loader.append($image);
        this.element.append($loader);
        this.loader = $loader;
    };

    Sequence.prototype.loadSprite = function (index) {
        var sequence = this;
        var options = this.options;
        var i = index;
        var _image = options.path + options.files[i].file;
        var $image = $('<img>').attr('src', _image);
        var file = options.files[i];
        $image.appendTo(sequence.images.parent());
        $image.hide();
        $image.on('load', function () {
            sequence.createFromSprite($image, _image, file, i);
            if (++i < options.files.length) {
                sequence.loadSprite(i);
            } else {
                sequence.showSequence();
                if (options.responsive) {
                    sequence.createObserver();
                }
            }
        });
    };

    Sequence.prototype.createObserver = function () {
        var sequence = this;
        var element = this.element;
        var options = sequence.options;
        var width = sequence.options.width;
        var height = sequence.options.height;
        checkRatio();

        $(window).resize(function () {
            checkRatio();
        });

        function checkRatio() {
            var frame = sequence.frames[sequence.currentFrame];
            if (frame.width() !== width) {
                var ratio = frame.width() / width;
                var _height = height * ratio;
                $('.' + options.classCommon).css('height', _height);
                element.css('height', _height);
            } else {
                $('.' + options.classCommon).css('height', height);
                element.css('height', height);
            }
        }
    };

    Sequence.prototype.createFromSprite = function ($image, path, file, index) {
        var sequence = this;
        var options = this.options;
        var $images = this.images;

        for (var j = file.start; j <= file.stop; j++) {
            var $li = $('<li></li>');
            $li.addClass(options.classCommon);
            $li.addClass(options.classGlobal + options.classSeparator + index);
            $li.addClass(options.className + options.classSeparator + pad(j, options.classPad));
            $li.appendTo($images);
            sequence.frames.push($li);
        }

        //$image.remove();
        $('li.' + options.classGlobal + options.classSeparator + index).css({
            'background-image': 'url("' + path + '")'
        });

        function pad(num, size) {
            var s = num + "";
            while (s.length < size)
                s = "0" + s;
            return s;
        }
    };

    Sequence.prototype.loadImages = function (currentImage) {
        var sequence = this;
        var options = this.options;
        var $li = $('<li></li>');
        var _image = options.path + options.name + pad(currentImage, this.options.numpad) + '.' + options.extension;
        var $image = $('<img>').attr('src', _image);
        $image.appendTo($li);
        this.frames.push($image);
        this.images.append($li);
        $image.hide();
        $($image).load(function () {
            sequence.imageLoaded();
        });

        function pad(num, size) {
            var s = num + "";
            while (s.length < size)
                s = "0" + s;
            return s;
        }

    };

    Sequence.prototype.imageLoaded = function () {
        var sequence = this;
        sequence.loadedImages += 1;
        if (sequence.loadedImages !== sequence.options.toLoad) {
            sequence.loadImages(sequence.loadedImages);
        } else {
            sequence.showSequence();
        }
    };

    Sequence.prototype.showSequence = function () {
        if (this.loader !== false) {
            this.loader.fadeOut(function () {
                $(this).remove();
            });
        }
        if (this.options.callback !== false) {
            this.options.callback();
        }
        if (this.options.firstFrame !== 0) {
            this.currentFrame = parseInt(this.options.firstFrame);
        }
        var frames = this.frames;
        if (frames.length > 0) {
            frames[this.currentFrame].css({opacity: 1});
        }
    };

    Sequence.prototype.animateFrame = function (index) {
        var frames = this.frames;
        if (frames[index]) {
            this.animateTo = index;
            this.start();
        } else {
            this.animateTo = frames.length - 1;
            this.start();
        }
    };
    
    Sequence.prototype.jumpFrame = function (index) {
        var frames = this.frames;
        if (frames[index]) {
            this.hideFrame();
            this.currentFrame = index;
            this.showFrame();
        }
    };

    Sequence.prototype.start = function () {
        var sequence = this;
        if (sequence.interval === 0) {
            sequence.interval = window.setInterval(function () {
                sequence.animate();
            }, Math.round(1000 / 35));
        }
    };

    Sequence.prototype.animate = function () {
        if (this.currentFrame !== this.animateTo) {
            var easing = this.animateTo < this.currentFrame ? Math.floor((this.animateTo - this.currentFrame) * 0.01) : Math.ceil((this.animateTo - this.currentFrame) * 0.01);
            this.hideFrame();
            this.currentFrame += easing;
            this.showFrame();
        } else {
            window.clearInterval(this.interval);
            this.interval = 0;
        }
    };

    Sequence.prototype.hideFrame = function () {
        this.frames[this.getCurrentFrame()].css({opacity: 0});
    };

    Sequence.prototype.showFrame = function () {
        this.frames[this.getCurrentFrame()].css({opacity: 1});
    };


    Sequence.prototype.getCurrentFrame = function () {
        var c = Math.ceil(this.currentFrame % this.frames.length);
        if (c < 0)
            c += (this.frames.length - 1);
        return c;
    };

    function Plugin(option, additional) {
        if (typeof option === 'string') {
            if ($(this).data('sequence') && option === 'animateFrame') {
                return $(this).data('sequence').animateFrame(additional);
            }
            if ($(this).data('sequence') && option === 'jumpFrame') {
                return $(this).data('sequence').jumpFrame(additional);
            }
        }
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('sequence');
            var options = $.extend({}, Sequence.defaults, option);

            if (!data) {
                $this.data('sequence', (data = new Sequence(this, options)));
            }
        });
    }

    $.fn.sequence = Plugin;
    $.fn.sequence.Constructor = Sequence;

}(jQuery));
