var ranges = [
    0, 41, 110, 174
];


$(document).ready(function() {
    
    if (navigator.appName.indexOf("Internet Explorer") != -1){
            var ieDetect = (
                    navigator.appVersion.indexOf("MSIE 9")==-1 &&
                    navigator.appVersion.indexOf("MSIE 1")==-1
            );

            if (ieDetect){
                    $('.alert').removeClass('hidden');
            } else {
                    $('.alert').remove();
            }
    } else {
            $('.alert').remove();
    }	    
    
    /*
     * Create FAQ Section
     */
    createFaq();
    
    var naviScroll = false;

    $('#main-navigation li a').click(function(ev) {
        ev.preventDefault();
        naviScroll = true;
        var $this = $(this);
        $('#main-navigation').css({
           'background-position': '0px ' + $this.parent('li').position().top + 'px'
        });
        $('#main-navigation .active').removeClass('active');
        $(this).parent('li').addClass('active');
        
        var options = {
            animation: {
                complete: function() { naviScroll = false; },
                duration: 1000,
            }
        };
        if ($(window).width() < 768) {
            options.gap = {
                y: $('.navbar-nfc .navbar-header').height() * (-1)                
            };
            if ($('#main-menu').hasClass('in')) {
                $('#main-menu').collapse('hide');
                var hash = this.hash;
                $('#main-menu').on('hidden.bs.collapse', function(){
                    $('html,body').scrollTo(hash, hash, options);
                });
            } else {
                $('html,body').scrollTo(this.hash, this.hash, options);
            }
        } else {
            $('html,body').scrollTo(this.hash, this.hash, options);
        }
    });

//    $('#soft').noUiSlider({
//        start: 0,
//        range: {
//            min: ranges[0],
//            '33%': ranges[1],
//            '66%': ranges[2],
//            max: ranges[3]
//        }
//    });
//    
//    $('#nfc-sequence').click(function(ev) {
//       ev.preventDefault();
//       var val = Math.floor(Number($('#soft').val()));
//       var breakpoint;
//       for (var i = 0; i < ranges.length; i++) {
//           if (val < ranges[i]) {
//               breakpoint = ranges[i];
//               $('#soft').val(breakpoint);
//               break;
//           }
//       }
//    });

//    $('#soft').on({
//       slide: function(ev, v) {
//           $('#nfc-sequence').sequence('animateFrame', Math.floor(v));
//           $.each(noUiMarkers, function(){
//              if ($(this).data('value') < v) {
//                  $(this).addClass('noUi-marker-active');
//              } else {
//                  $(this).removeClass('noUi-marker-active');
//              }
//           });
//           $.each(noUiValue, function(){
//               if (v > $(this).data('value') + 10) {
//                   $(this).removeClass('noUi-value-active');
//               } else if (v < ($(this).data('value') + 10) && (parseInt(v) + 10) > $(this).data('value')) {
//                   $(this).addClass('noUi-value-active');
//               } else {
//                   $(this).removeClass('noUi-value-active');
//               }
//           });
//       },
//       set: function(ev, v) {
//           $(this).trigger('slide', v);
//       }
//    });
    
    

//    $('#soft').noUiSlider_pips({
//        mode: 'values',
//        values: [0, 54, 84, 136, 214],
//        labels: ['Otwórz swój portfel', 'Zamień kartę plastikową na kartę NFC', 'Włącz w telefonie antenę NFC i aplikację płatniczą Banku', 'Zbliż telefon do czytnika i zapłać', 'Teraz zawsze płać telefonem za swoje zakupy!'],
//        density: 100,
//	stepped: true
//    });
    
//    var noUiValue = $('.noUi-value-large');
//    var noUiMarkers = $('.noUi-marker-large');
//    
//    noUiValue.first().addClass('noUi-value-active');

    $('#nfc-sequence').sequence({
        toLoad: 175,
        start: 0,
        path: 'seq/',
        files: [
            {
                file: 'seq_01.jpg',
                start: 0,
                stop: 9
            },
            {
                file: 'seq_02.jpg',
                start: 10,
                stop: 19
            },
            {
                file: 'seq_03.jpg',
                start: 20,
                stop: 29
            },
            {
                file: 'seq_04.jpg',
                start: 30,
                stop: 39
            },
            {
                file: 'seq_05.jpg',
                start: 40,
                stop: 49
            },
            {
                file: 'seq_06.jpg',
                start: 50,
                stop: 59
            },
            {
                file: 'seq_07.jpg',
                start: 60,
                stop: 69
            },
            {
                file: 'seq_08.jpg',
                start: 70,
                stop: 79
            },
            {
                file: 'seq_09.jpg',
                start: 80,
                stop: 89
            },
            {
                file: 'seq_10.jpg',
                start: 90,
                stop: 99
            },
            {
                file: 'seq_11.jpg',
                start: 100,
                stop: 109
            },
            {
                file: 'seq_12.jpg',
                start: 110,
                stop: 119
            },
            {
                file: 'seq_13.jpg',
                start: 120,
                stop: 129
            },
            {
                file: 'seq_14.jpg',
                start: 130,
                stop: 139
            },
            {
                file: 'seq_15.jpg',
                start: 140,
                stop: 149
            },
            {
                file: 'seq_16.jpg',
                start: 150,
                stop: 159
            },

            {
                file: 'seq_17.jpg',
                start: 160,
                stop: 169
            },
            {
                file: 'seq_18.jpg',
                start: 170,
                stop: 174
            }
        ],
        method: 'sprite',
        width: '500',
        height: '400',
        responsive: true,
        render: 'html',
        loader: 'images/loader.gif',
        background: '#FFF',
        firstFrame: ranges[3],
        callback: function(){
            $('#slider').removeClass('loading');
            $('.glyphicon-play').removeClass('hidden');
            $('.glyphicon-play').click(function(ev){
                $('#nfc-sequence').sequence('jumpFrame', ranges[0]);
                //$(this).fadeTo(500, 0, function(){
                    $(this).addClass('hidden');
                    setTimeout(function(){
                       $('#nfc-sequence').sequence('animateFrame', ranges[1]);
                        setTimeout(function(){
                            $('#nfc-sequence').sequence('animateFrame', ranges[2]);
                            setTimeout(function(){
                                $('#nfc-sequence').sequence('animateFrame', ranges[3]);
                                setTimeout(function(){
                                   $('.glyphicon-play').removeClass('hidden'); 
                                }, 2500);
                            }, 3000);
                        }, 2500);                
                    }, 500);                                                        
                });
            //});
        }
    });

    $('.mobile-control').click(function(ev){
        ev.preventDefault();
        var isTerminal = $(this).attr('data-terminal');
        if(isTerminal){
            $('#mobile-presentation').addClass("terminal");
        }else{
            $('#mobile-presentation').removeClass("terminal");
        }
        var $target = $(this).attr('href');
        $('.mobile-view.active').removeClass('active');
        $($target).addClass('active');
        $('.mobile-control.current').removeClass('current');
        $(this).addClass('current');
    });

    $('.operator-toggle').change(function() {
        $('#mobile-presentation').removeClass("terminal");
        $('.operator-toggle').prop('disabled', true);
        var open = $(this).data('controls');
        $('.mobile-control.current').removeClass('current');
        $('.mobile-controls[data-controls="' + open + '"]').on('shown.bs.collapse', function(){
           $('.operator-toggle').prop('disabled', false); 
        });
        if ($('.mobile-controls.in').length > 0) {
            var $current = $('.mobile-controls.in');
            $current.on('hidden.bs.collapse', function() {
                $('.mobile-controls[data-controls="' + open + '"]').collapse('show');
            });
            showFirstSlide();
            $current.collapse('hide');
        } else {
            $('.mobile-controls[data-controls="' + open + '"]').collapse('show');
            showFirstSlide();
        }
        
        function showFirstSlide() {
            $('.mobile-view.active').removeClass('active');
            $('.mobile-view').first().addClass('active');
        }
        
    });
    
    $('.read-more').on('show.bs.collapse', function() {
       $('.read-more.in').collapse('hide'); 
    });

    $('.read-more').on('shown.bs.collapse', function () {
        var id = '#' + $(this).attr('id');
        if ($(window).width() < 768) {
            var options = {
                gap: {
                    y: $('.navbar-nfc .navbar-header').height() * (-1) - 100
                },
                animation: {
                    complete: function() {
                        naviScroll = false;
                    }
                }
            };
            $('html,body').scrollTo(id, id, options);
        }
    });

    // $('#section_2').mousemove(function(e){
    //    rotate(e.pageX);
    // });


    function rotate(deg) {
        var degrees = ((deg - $(window).width() / 2) / 100);
        $('.icons.up').css({
      '-webkit-transform' : 'rotateY('+degrees+'deg)',
         '-moz-transform' : 'rotateY('+degrees+'deg)',
          '-ms-transform' : 'rotateY('+degrees+'deg)',
           '-o-transform' : 'rotateY('+degrees+'deg)',
              'transform' : 'rotateY('+degrees+'deg)',
                   'zoom' : 1

        });
    }
    
        
    $('.panel-collapse').on('show.bs.collapse', function(){
        $('.panel-collapse.in').collapse('hide');
    });
    
    $('body').attr('data-spy', 'scroll');
    $('body').attr('data-target', '#main-menu');
    
    if ($(window).width() >= 768) {  
        $('body').attr('data-offset', 0);
    } else {
        $('body').attr('data-offset', $('.navbar-header').height() + 20);
    }
    
    //$('body').scrollspy({target: '#main-menu'});
    
    $(window).resize(function(){
        if ($(window).width() >= 768) { 
            $('body').attr('data-offset', 0);
            $('body').scrollspy('refresh');
        } else {
            $('body').attr('data-offset', $('.navbar-header').height() + 20);
            $('body').scrollspy('refresh');
        }
        /*
        * Check "read more" in section_3
        */
        readMore();        
    });
    
    $('#main-menu').on('activate.bs.scrollspy', function(ev) {
        if (!naviScroll) {
            var $target = $(ev.target);
            $('#main-navigation').css({
                'background-position': '0px ' + $target.position().top + 'px'
            });
        }
    });
    
});

$(window).load(function(){
    /*
     * Check images in contact
     */
    checkRetina();   
    
    /*
     * Check "read more" in section_3
     */
    readMore();
    
});

/*
 * Read More Section
 */
function readMore() {
    if ($(window).width() > 768 && $('#section_3').data('mobile') === true) {
        $.each($('#section_3 .read-more'), function(){
            var $this = $(this);
            var id = $this.attr('id');
            var $target = $('#section_need .read-more[data-id="' + id + '"]');
            $this.removeAttr('id');
            $target.attr('id', id);
            $this.children('.row').html('');
            $('#section_3').data('mobile', false);
            $('.read-more.in').collapse('hide'); 
        });
    }
    if ($(window).width() <= 768 && $('#section_3').data('mobile') === false) {
        $.each($('#section_need .read-more'), function(){
            var $this = $(this);
            var id = $this.attr('id');
            var $target = $('#section_3 .read-more[data-id="' + id + '"]');
            $this.removeAttr('id');
            $target.children('.row').append($this.html());
            $target.attr('id', id);
            $target.addClass('collapse');
            $('#section_3').data('mobile', true);
            $('.read-more.in').collapse('hide'); 
        });
    } 
}

/*
 * Create FAQ Section
 */
function createFaq() {
    var prototype = '<div class="panel panel-nfc">' +
                          '<div class="panel-heading" role="tab">' +
                              '<h4 class="panel-title">' +
                                  '<a class="collapsed" data-toggle="collapse" data-parent="__parent__" href="#__collapse__id__" aria-expanded="false" aria-controls="__collapse__id__">' +
                                      '<span>__question__</span>' +
                                  '</a>' +
                              '</h4>' +
                          '</div>' +
                          '<div id="__collapse__id__" class="panel-collapse collapse" role="tabpanel">' +
                              '<div class="panel-body">' +
                              '__answer__' +
                              '</div>' +
                          '</div>' +
                      '</div>';
    $.each(faq, function(i){
        var $faq = prototype;
        $faq = $faq
                .replace(/__parent__/, this.parent)
                .replace(/__collapse__id__/g, 'collapse_'+i)
                .replace(/__question__/, this.question)
                .replace(/__answer__/, this.answer);
        $(this.parent).append($faq);
    });
};

function checkRetina() {
    if (window.devicePixelRatio > 1) {
        $.each($('.retina'), function(){
           var height = $(this).height();
           var retina = $(this).data('retina');
           $(this).attr('src', retina);
           $(this).css({
               'max-height': height
           });
        });
    }
}

if (!Modernizr.touch) {
    $(function() {
        var scrollMagicController = new ScrollMagic();

        /*
         * Czego potrzebujesz, aby płacić zbliżeniowo
         */
        var Section3 = function() {
            var tweens = [
                {
                    tween: TweenMax.from('#mobile-nfc', 0.75, {css: {top: 100, opacity: 0}, ease: Power2.easeInOut})
                },
                {
                    tween: TweenMax.from('#sim-nfc', 0.75, {css: {top: 100, opacity: 0}, ease: Power2.easeInOut}),
                    time: 0.15
                },
                {
                    tween: TweenMax.from('#bph-nfc', 0.75, {css: {top: 100, opacity: 0}, ease: Power2.easeInOut}),
                    time: 0.3
                },
                {
                    tween: TweenMax.from('.nfc-shadow', 0.3, {opacity: 0}),
                    time: 0.7
                },
                {
                    tween: TweenMax.from(['.nfc-description', '.need-button'], 0.4, {opacity: 0}),
                    time: 0.7
                }
            ];

            createScene('#section_3', 0, tweens);
        };
        
        /*
         * Jak to działa
         */
        // var Section4 = function() {
        //     var tweens = [
        //         {
        //             tween: TweenMax.from('#mobile-presentation', 0.75, {css: {top: 100, opacity: 0}, ease: Power2.easeInOut})
        //         },
        //         {
        //             tween: TweenMax.from('#operators-holder', 0.5, {css: {opacity: 0}, ease: Power2.easeInOut}),
        //             time: 0.25
        //         }
        //     ];
        //     createScene('#section_4', 0, tweens);
        // };
        
        /*
         * Płać swoim telefonem NFC gdzie chcesz i kiedy chcesz
         */
        // var Section2 = function() {
        //     var tweens = [
        //         {
        //             tween: TweenMax.from('#icon-5', 1.2, {css: {top: +800}, ease: Back.easeOut.config(1)})
        //         },
        //         {
        //             tween: TweenMax.from('#icon-2', 0.95, {css: {top: +800}, ease: Back.easeOut.config(1)}),
        //             time: 0.1
        //         },
        //         {
        //             tween: TweenMax.from('#icon-3', 0.95, {css: {top: +800}, ease: Back.easeOut.config(1.5)}),
        //             time: 0.3
        //         },
        //         {
        //             tween: TweenMax.from('#icon-6', 1, {css: {top: +800}, ease: Back.easeOut.config(1.75)}),
        //             time: 0.4
        //         },
        //         {
        //             tween: TweenMax.from('#icon-4', 1.1, {css: {top: +800}, ease: Back.easeOut.config(1)}),
        //             time: 0.45
        //         },
        //         {
        //             tween: TweenMax.from('#icon-1', 1.2, {css: {top: +800}, ease: Back.easeOut.config(0.7)}),
        //             time: 0.5
        //         },

        //         {
        //             tween: TweenMax.from('#icon-11', 0.8, {css: {top: +800}, ease: Back.easeOut.config(1)}),
        //             time: 0.6
        //         },
        //         {
        //             tween: TweenMax.from('#icon-8', 0.7, {css: {top: +800}, ease: Back.easeOut.config(0.8)}),
        //             time: 0.7
        //         },
        //         {
        //             tween: TweenMax.from('#icon-9', 1, {css: {top: +800}, ease: Back.easeOut.config(0.9)}),
        //             time: 0.75
        //         },
        //         {
        //             tween: TweenMax.from('#icon-10', 0.95, {css: {top: +800}, ease: Back.easeOut.config(0.6)}),
        //             time: 0.85
        //         },
        //         {
        //             tween: TweenMax.from('#icon-7', 1.2, {css: {top: +800}, ease: Back.easeOut.config(1)}),
        //             time: 1
        //         }                
                                
        //     ];
        //     createScene('#section_2', 0, tweens);
        // };
        
        var Section7 = function() {
            var tweens = [
                {
                    tween: TweenMax.from('#contact-telephone', 1.5, {css: {top: +200, opacity: 0}, ease: Power2.easeInOut })
                },
                {
                    tween: TweenMax.from('#contact-local', 1.5, {css: {top: +200, opacity: 0}, ease: Power2.easeInOut }),
                    time: 0.1
                },
                {
                    tween: TweenMax.from('#contact-form', 1.5, {css: {top: +200, opacity: 0}, ease: Power2.easeInOut }),
                    time: 0.2
                }
            ];
            createScene('#section_contact', -130, tweens);
        };
        
        var animationsCreated = false;
        
        if ($(window).width() >= 768) {
            createAnimations();
        }
        
        function createAnimations() {
            Section3();
            // Section4();
            // Section2();
            Section7();
            animationsCreated = true;            
        }
        
        function createScene(element, offset, tweens, removeReverse) {
            var timeline = new TimelineMax();
            for(var i = 0; i < tweens.length; i++) {
                if (tweens[i].time) {
                    timeline.add(tweens[i].tween, tweens[i].time);
                } else {
                    timeline.add(tweens[i].tween);
                }
            }
            var scene = new ScrollScene({
                triggerElement: element,
                offset: offset
            })
                    .setTween(timeline)
                    .addTo(scrollMagicController);

            /*scene.addIndicators({
                'zindex': 1001
            });*/
            if (removeReverse) {
                scene.reverse(false);
            }
        };
        
        if ($(window).width() < 768) {
            scrollMagicController.enabled(false);
        }
        
        $(window).resize(function(){
           if ($(window).width() < 768) {
               scrollMagicController.enabled(false);
           } else {
               if (!scrollMagicController.enabled()) {
                   scrollMagicController.enabled(true);
               }
               if (!animationsCreated) {
                   createAnimations();
               }
           }
        });
        
    });
}